package hotciv.variables;

/**
 * Created by magni on 13-12-2016.
 */

import hotciv.framework.GameConstants;
import hotciv.framework.Tile;
import hotciv.standard.WorldTile;
import hotciv.variables.strategies.BuildWorldStrategy;
import thirdparty.ThirdPartyFractalGenerator;


public class FractalGeneratorAdapter implements BuildWorldStrategy {

    Tile[][] tileMap = new Tile[16][16];
    ThirdPartyFractalGenerator generator =
            new ThirdPartyFractalGenerator();

    @Override
    public Tile[][] buildWorld() {


        for ( int r = 0; r < 16; r++ ) {
            for ( int c = 0; c < 16; c++ ) {
                String tile = "" + generator.getLandscapeAt(r,c);
                if (tile.equals("M")){
                    tileMap[r][c] = new WorldTile(GameConstants.MOUNTAINS);
                }if (tile.equals("o")){
                    tileMap[r][c] = new WorldTile(GameConstants.PLAINS);
                }if (tile.equals("f")){
                    tileMap[r][c] = new WorldTile(GameConstants.FOREST);
                }if (tile.equals(".")){
                    tileMap[r][c] = new WorldTile(GameConstants.OCEANS);
                }if (tile.equals("h")){
                    tileMap[r][c] = new WorldTile(GameConstants.HILLS);
                }
            }

        }
        return tileMap;
    }

}
