package hotciv.visual;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.view.GfxConstants;
import minidraw.framework.DrawingEditor;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Created by Karl-Emil on 16-12-2016.
 */
public class SetFocusTool implements CompositionToolComponent {
    private final DrawingEditor editor;
    private final Game game;

    public SetFocusTool(Game game, DrawingEditor editor) {
        this.editor = editor;
        this.game = game;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {
        Position p = GfxConstants.getPositionFromXY(i, i1);
        if(p.getRow() > 16 || p.getColumn() > 16){ return; }
        game.setTileFocus(p);

   }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {

    }

    @Override
    public void addTool(CompositionToolComponent tool) {
        throw new NotImplementedException();
    }
}
