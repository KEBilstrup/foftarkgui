package hotciv.variables.strategies;

import hotciv.framework.Game;
import hotciv.framework.Player;

/**
 * Created by magni on 17-11-2016.
 */
public interface WinningStrategies {

    public Player getWinner(Game game);
}
