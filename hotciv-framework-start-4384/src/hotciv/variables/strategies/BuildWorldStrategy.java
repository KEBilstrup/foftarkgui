package hotciv.variables.strategies;

import hotciv.framework.Tile;

/**
 * Created by magni on 13-12-2016.
 */
public interface BuildWorldStrategy {

    public Tile[][] buildWorld();

}
