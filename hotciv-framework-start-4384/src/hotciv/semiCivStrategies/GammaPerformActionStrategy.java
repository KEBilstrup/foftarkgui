package hotciv.semiCivStrategies;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.standard.ArcherUnit;
import hotciv.standard.GameImpl;
import hotciv.standard.PlayerCity;
import hotciv.variables.strategies.PerformActionStrategies;

/**
 * Created by magni on 18-11-2016.
 */
public class GammaPerformActionStrategy implements PerformActionStrategies {
    @Override
    public void performActionOn(Game game, Position p) {
        GameImpl gameImpl = (GameImpl) game;
        Player owner = game.getUnitAt(p).getOwner();
        if (game.getUnitAt(p).getTypeString().equals(GameConstants.SETTLER)){
            gameImpl.setUnit(p, null);
            gameImpl.setCity(p, new PlayerCity(owner));
            return;
        }

        if (game.getUnitAt(p).getTypeString().equals(GameConstants.ARCHER)){
            ArcherUnit archerUnit = (ArcherUnit) game.getUnitAt(p);
            if(!archerUnit.isFortified()) {
                archerUnit.setDefensiveStrength(archerUnit.getDefensiveStrength() * 2);
                archerUnit.setFortified(true);
                return;
            } else {
                archerUnit.setDefensiveStrength(archerUnit.getDefensiveStrength() /2);
                archerUnit.setFortified(false);
                return;
            }
        }
    }
}
