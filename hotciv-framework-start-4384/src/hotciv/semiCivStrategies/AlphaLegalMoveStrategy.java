package hotciv.semiCivStrategies;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.standard.PlayerCity;
import hotciv.variables.strategies.LegalMoveStrategy;

/**
 * Created by Karl-Emil on 19-12-2016.
 */
public class AlphaLegalMoveStrategy implements LegalMoveStrategy {
    @Override
    public boolean isLegalMove(Game game, Position from, Position to) {
        if (game.getUnitAt(from).getOwner() != game.getPlayerInTurn()) {
            return false;
        }
        if(game.getUnitAt(from).getMoveCount() <= 0){
            return false;
        }

        int x = from.getRow();
        int y = from.getColumn();
        boolean moveOnlyOneTile = false;

        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                Position nearPosition = new Position(i, j);
                if (nearPosition.equals(to)) {
                    moveOnlyOneTile = true;
                }
            }
        }
        if (!moveOnlyOneTile) {
            return false;
        }

        if (game.getCityAt(to) != null) {
            PlayerCity testCity = (PlayerCity) game.getCityAt(to);
            testCity.setOwner(game.getUnitAt(from).getOwner());
        }


        //First we check if we try to move the unit to a place we are not allowed
        if (game.getTileAt(to).getTypeString().equals(GameConstants.OCEANS) || game.getTileAt(to).getTypeString().equals(GameConstants.MOUNTAINS)) {
            return false;
        }

        if (game.getUnitAt(to) == null) {
            return true;
        }

        //Check if there is a unit from the same owner on new position already
        if (game.getUnitAt(to).getOwner().equals(game.getUnitAt(from).getOwner())) {
            return false;
        }

        return true; //Attack!
    }
}
