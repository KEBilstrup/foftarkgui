package hotciv.visual;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.view.GfxConstants;
import minidraw.framework.DrawingEditor;
import minidraw.framework.Tool;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Created by ThinkNick on 19-12-2016.
 */
public class ActionTool implements CompositionToolComponent {

    private final Game game;
    private final DrawingEditor editor;
    private Position p;
    public ActionTool(DrawingEditor editor, Game game){
        this.game = game;
        this.editor = editor;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
        p = GfxConstants.getPositionFromXY(i, i1);
        if(p.getRow() > 16 || p.getColumn() > 16){ return; }
        if(game.getUnitAt(p) == null) { return; }
        if(mouseEvent.isShiftDown()){
            game.performUnitActionAt(p);
        }
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {


    }

    @Override
    public void addTool(CompositionToolComponent tool) {
        throw new NotImplementedException();
    }
}
