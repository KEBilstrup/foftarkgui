package hotciv.visual;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.framework.Unit;
import hotciv.view.GfxConstants;
import minidraw.framework.Drawing;
import minidraw.framework.DrawingEditor;
import minidraw.framework.Figure;
import minidraw.framework.Tool;
import minidraw.standard.AbstractTool;
import minidraw.standard.SelectionTool;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Created by Karl-Emil on 16-12-2016.
 */
public class MoveUnitTool implements CompositionToolComponent {
    private SelectionTool selectionTool;
    private Game game;
    private Position from;
    private Position to;
    private Figure draggedFigure;

    public MoveUnitTool(Game game, DrawingEditor editor) {
        selectionTool = new SelectionTool(editor);
        this.game = game;

        from = new Position(0,0);
    }


    @Override
    public void mouseDown(MouseEvent mouseEvent, int x, int y) {

        Position pos = GfxConstants.getPositionFromXY(x,y);
        if(pos.getRow() > 16 || pos.getColumn() > 16){ return; }

        if(game.getUnitAt(pos) != null) {
            from = pos;
            System.out.println("DU HAR KLIKKET PÅ EN UNIT");
            selectionTool.mouseDown(mouseEvent, x, y);
        }
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int x, int y) {
        selectionTool.mouseDrag(mouseEvent, x, y);
    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int x, int y){
        to = GfxConstants.getPositionFromXY(x,y);
        if(to.getRow() > 16 || to.getColumn() > 16){ return; }
        if(game.getUnitAt(from) != null) {
            if (!game.moveUnit(from, to)) {
                System.out.println("Du KAN IKKE FLYTTE HERHEN");
            }
            //selectionTool.mouseUp(mouseEvent, x, y);
        }

        from = new Position(0,0);
        to = null;
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {
    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {
    }

    @Override
    public void addTool(CompositionToolComponent tool) {
        throw new NotImplementedException();
    }
}
