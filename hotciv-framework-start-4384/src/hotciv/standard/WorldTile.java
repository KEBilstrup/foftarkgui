package hotciv.standard;

import hotciv.framework.Tile;

/**
 * Created by Karl-Emil on 04-11-2016.
 */
public class WorldTile implements Tile {

    private final String tileType;

    public WorldTile(String type){
        tileType = type;

    }

    @Override
    public String getTypeString() {
        return tileType;
    }
}
