package hotciv.semiCivStrategies;

import hotciv.framework.City;
import hotciv.framework.Player;
import hotciv.standard.PlayerCity;
import hotciv.variables.strategies.InitCitiesStrategies;

/**
 * Created by Karl-Emil on 18-11-2016.
 */
public class DeltaInitCityStrategy implements InitCitiesStrategies {


    @Override
    public City[][] initCities() {
        City[][] citymap = new City[16][16];
        citymap [4][5] = new PlayerCity(Player.BLUE);
        citymap [8][12] = new PlayerCity(Player.RED);
        return citymap;
    }
}
