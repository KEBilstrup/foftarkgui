package hotciv.standard;

import hotciv.framework.Player;

/**
 * Created by magni on 25-11-2016.
 */
public class LegionUnit extends PlayerUnit {
    private int defensiveStrength = 2;
    private int attackingStrength = 4;

    public LegionUnit(Player owner, String type) {
        super(owner, type);
    }

    @Override
    public int getAttackingStrength() {
        return attackingStrength;
    }

    @Override
    public int getDefensiveStrength() {
        return defensiveStrength;
    }

    @Override
    public int getProductionCost() {
        return 15;
    }
}
