package hotciv.standard;

import hotciv.framework.Player;

/**
 * Created by Karl-Emil on 02-12-2016.
 */
public class BombUnit extends PlayerUnit {
    private int defensiveStrength = 1;
    private int attackingStrength = 0;

    public BombUnit(Player owner, String type) {
        super(owner, type);
    }

    @Override
    public int getAttackingStrength() {
        return attackingStrength;
    }

    @Override
    public int getDefensiveStrength() {
        return defensiveStrength;
    }

    @Override
    public int getProductionCost() {
        return 60;
    }
}
