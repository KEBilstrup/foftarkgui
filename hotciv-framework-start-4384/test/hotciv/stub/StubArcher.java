package hotciv.stub;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Unit;

/**
 * Created by magni on 20-12-2016.
 */
public class StubArcher implements Unit{

    private int defensiveStrength = 2;
    private int moveCount = 1;
    private Player owner;
    private boolean isFortified;

    public StubArcher(Player owner){
        this.owner = owner;
    }

    public void fortified(boolean isFortified){
        System.out.println("DU HAR TRYKKET PERFORM ACTION");
        if(isFortified){
            defensiveStrength = defensiveStrength * 2;
            moveCount = 0;
            this.isFortified = isFortified;
        }else{
            defensiveStrength = defensiveStrength / 2;
            moveCount = 1;
            this.isFortified = isFortified;
        }
    }

    @Override
    public String getTypeString() {
        return GameConstants.ARCHER;
    }

    @Override
    public Player getOwner() {
        return owner;
    }

    @Override
    public int getMoveCount() {
        return moveCount;
    }

    @Override
    public int getDefensiveStrength() {
        return defensiveStrength;
    }

    @Override
    public int getAttackingStrength() {
        return 0;
    }

    public boolean isFortified() {
        return isFortified;
    }
}
