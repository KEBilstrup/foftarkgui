package hotciv.standard;

import hotciv.framework.Player;

/**
 * Created by Karl-Emil on 18-11-2016.
 */
public class ArcherUnit extends PlayerUnit {

    private boolean fortified;
    private int defensiveStrength = 3;
    private int attackingStrength = 2;

    public ArcherUnit(Player owner, String type) {
        super(owner, type);
        fortified = false;
    }

    public void setFortified(boolean isFortified){
        if(isFortified) {
            defensiveStrength = 2 * defensiveStrength;
            decrementMoveCount();
        }else{
            defensiveStrength = defensiveStrength / 2;
            incrementMoveCount();
        }
        fortified = isFortified;
    }

    public boolean isFortified() {
        return fortified;
    }

    @Override
    public int getAttackingStrength() {
        return attackingStrength;
    }

    @Override
    public int getDefensiveStrength() {
        return defensiveStrength;
    }

    @Override
    public int getProductionCost() {
        return 10;
    }

}
