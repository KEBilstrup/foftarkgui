package hotciv.variables.strategies;

import hotciv.framework.Game;
import hotciv.framework.Position;

/**
 * Created by Karl-Emil on 19-12-2016.
 */
public interface LegalMoveStrategy {
    public boolean isLegalMove(Game game, Position from, Position to);
}
