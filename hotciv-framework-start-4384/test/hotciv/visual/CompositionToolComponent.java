package hotciv.visual;

import hotciv.framework.Game;
import minidraw.framework.DrawingEditor;
import minidraw.framework.Tool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * Created by magni on 20-12-2016.
 */
public interface CompositionToolComponent extends Tool {
    void addTool(CompositionToolComponent tool);
}
