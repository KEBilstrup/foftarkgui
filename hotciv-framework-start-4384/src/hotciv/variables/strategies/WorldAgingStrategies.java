package hotciv.variables.strategies;

/**
 * Created by magni on 17-11-2016.
 */
public interface WorldAgingStrategies {
    public int calculateAge(int gameAge);
}
