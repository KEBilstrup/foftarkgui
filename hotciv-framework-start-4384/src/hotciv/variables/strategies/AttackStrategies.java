package hotciv.variables.strategies;

import hotciv.framework.Game;
import hotciv.framework.Position;

/**
 * Created by magni on 24-11-2016.
 */
public interface AttackStrategies {
    public boolean attack(Position attacking, Position defending, Game game);
}
