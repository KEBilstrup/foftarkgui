package hotciv.semiCivStrategies;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.standard.AdditionalGameConstants;
import hotciv.standard.GameImpl;
import hotciv.variables.strategies.PerformActionStrategies;

/**
 * Created by Karl-Emil on 02-12-2016.
 */
public class ThetaPerformActionStrategy implements PerformActionStrategies {
    GammaPerformActionStrategy gammaPerformActionStrategy;

    public ThetaPerformActionStrategy() {
        gammaPerformActionStrategy = new GammaPerformActionStrategy();
    }

    @Override
    public void performActionOn(Game game, Position position) {

        if (game.getUnitAt(position).getTypeString().equals(AdditionalGameConstants.BOMB)) {

            GameImpl gameImpl = (GameImpl) game;
            int x = position.getRow();
            int y = position.getColumn();
            for (int i = x - 1; i <= x + 1; i++) {
                for (int j = y - 1; j <= y + 1; j++) {
                    gameImpl.setUnit(new Position(i,j), null);
                }
            }
        } else { gammaPerformActionStrategy.performActionOn(game, position);}
    }
}
