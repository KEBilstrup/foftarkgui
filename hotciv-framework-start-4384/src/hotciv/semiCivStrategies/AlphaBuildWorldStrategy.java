package hotciv.semiCivStrategies;

import hotciv.framework.GameConstants;
import hotciv.framework.Tile;
import hotciv.standard.WorldTile;
import hotciv.variables.strategies.BuildWorldStrategy;

/**
 * Created by magni on 13-12-2016.
 */
public class AlphaBuildWorldStrategy implements BuildWorldStrategy {

    @Override
    public Tile[][] buildWorld() {

        Tile[][] tilesMap = new Tile[16][16];
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                tilesMap[i][j] = new WorldTile(GameConstants.PLAINS);
            }
        }
        tilesMap[1][0] = new WorldTile(GameConstants.OCEANS);
        tilesMap[0][1] = new WorldTile(GameConstants.HILLS);
        tilesMap[2][2] = new WorldTile(GameConstants.MOUNTAINS);

        return tilesMap;
    }
}
