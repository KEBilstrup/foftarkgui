package hotciv.variables;

import hotciv.variables.strategies.*;

/**
 * Created by Karl-Emil on 29-11-2016.
 */
public interface AbstractFactory {

    public WinningStrategies getWinningStrategy();

    public WorldAgingStrategies getWorldAgingStrategy();

    public PerformActionStrategies getPerformActionStrategies();

    public InitCitiesStrategies getInitCitiesStrategies();

    public AttackStrategies getAttackStrategies();

    public BuildWorldStrategy getBuildWorldStrategy();

    public LegalMoveStrategy getLegalMoveStrategy();
}