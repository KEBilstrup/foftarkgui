package hotciv.standard;

import hotciv.framework.*;
import hotciv.variables.AbstractFactory;

import java.util.ArrayList;

/**
 * Created by Karl-Emil on 08-12-2016.
 */
public class GameImplTranscriptDecorator implements Game {
    private GameImpl gameImpl;

    public GameImplTranscriptDecorator(AbstractFactory factory){
        gameImpl = new GameImpl(factory);
    }

    public Tile getTileAt(Position p) {
        return gameImpl.getTileAt(p);
    }

    public int getAge() {
        return gameImpl.getAge();
    }

    public City getCityAt(Position p) {
        return gameImpl.getCityAt(p);
    }

    public void changeProductionInCityAt(Position p, String unitType) {
        gameImpl.changeProductionInCityAt(p, unitType);
        makeString("Change production to " + unitType);
    }

    public Unit getUnitAt(Position p) {
        return gameImpl.getUnitAt(p);
    }

    public boolean moveUnit(Position from, Position to) {
        String unit = gameImpl.getUnitAt(from).getTypeString();

        String appropriateMessage = "";
        if (gameImpl.getUnitAt(to) == null){
            appropriateMessage = "Move " + unit + " from " + from.toString() + " to " + to.toString();
        }else if (!gameImpl.getUnitAt(to).getOwner().equals(gameImpl.getUnitAt(from).getOwner())) {
            appropriateMessage = "Attacks " + gameImpl.getUnitAt(to).getTypeString() + " with " + gameImpl.getUnitAt(from).getTypeString();
        }
        if (!gameImpl.moveUnit(from, to)){
            return false;
        }else {
            makeString(appropriateMessage);
            return true;
        }
    }

    public Player getPlayerInTurn() {
        return gameImpl.getPlayerInTurn();
    }

    public Player getWinner() {
        if(gameImpl.getWinner() != null){
            System.out.println("The winner is " + gameImpl.getWinner().toString());
        }
        return gameImpl.getWinner();
    }

    public void endOfTurn() {
        makeString("Ends turn");
        gameImpl.endOfTurn();
    }

    public void performUnitActionAt(Position p) {
        makeString("Performs action of" + gameImpl.getUnitAt(p).getTypeString() + "on position " + p.toString());
        gameImpl.performUnitActionAt(p);
    }

    @Override
    public void addObserver(GameObserver observer) {
        gameImpl.addObserver(observer);
    }

    @Override
    public void setTileFocus(Position position) {
        gameImpl.setTileFocus(position);
    }

    public void changeWorkForceFocusInCityAt(Position p, String balance) {
        gameImpl.changeWorkForceFocusInCityAt(p, balance);
    }

    public void makeString(String message){
        System.out.print(gameImpl.getPlayerInTurn() + ": ");
        System.out.println(message);
    }

    public ArrayList<City> getAllCities() {
        return gameImpl.getAllCities();
    }

    public void setGameAge(int gameAge) {
        gameImpl.setGameAge(gameAge);
    }

    public int getPlayerRedAttacksWins() {
        return gameImpl.getPlayerRedAttacksWins();
    }

    public void incrementPlayerRedAttacksWins() {
        gameImpl.incrementPlayerRedAttacksWins();
    }

    public void resetPlayerAttacksWins() {
        gameImpl.resetPlayerAttacksWins();
    }

    public void setCity(Position p, City city) {
        gameImpl.setCity(p, city);
    }

    public void setTile(Position p, Tile tile) {
        gameImpl.setTile(p, tile);
    }

    public int getRounds() {
        return gameImpl.getRounds();
    }

    public void incrementPlayerBlueAttacksWins() {
        gameImpl.incrementPlayerBlueAttacksWins();
    }

    public void setUnit(Position p, Unit unit) {
        gameImpl.setUnit(p, unit);
    }

    public int getPlayerBlueAttacksWins() {
        return gameImpl.getPlayerBlueAttacksWins();
    }
}
