package hotciv.variables.strategies;

import hotciv.framework.City;
import hotciv.framework.Player;
import hotciv.standard.PlayerCity;

/**
 * Created by Karl-Emil on 18-11-2016.
 */
public class GeneralCitiesStrategy implements InitCitiesStrategies {

    @Override
    public City[][] initCities() {
        City[][] citymap = new City[16][16];
        citymap [4][1] = new PlayerCity(Player.BLUE);
        citymap [1][1] = new PlayerCity(Player.RED);
        return citymap;
    }
}
