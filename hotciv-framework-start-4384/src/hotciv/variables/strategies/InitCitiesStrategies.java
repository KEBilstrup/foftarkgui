package hotciv.variables.strategies;

import hotciv.framework.City;

/**
 * Created by Karl-Emil on 18-11-2016.
 */
public interface InitCitiesStrategies {

    public City[][] initCities();

}
