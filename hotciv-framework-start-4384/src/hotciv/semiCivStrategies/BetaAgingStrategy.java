package hotciv.semiCivStrategies;

import hotciv.variables.strategies.WorldAgingStrategies;

/**
 * Created by magni on 18-11-2016.
 */
public class BetaAgingStrategy implements WorldAgingStrategies {

    int age;

    @Override
    public int calculateAge(int gameAge) {
        this.age = gameAge;

        if (gameAge < -100){
            between4000And100BC();
        }else if (-100 <= gameAge && gameAge < 50){
            between100BCAnd50AD();
        }else if(50 <= gameAge && gameAge < 1750){
            between50And1750();
        }else if(1750 <= gameAge && gameAge < 1900){
            between1750And1900();
        }else if(1900 <= gameAge && gameAge < 1970){
            between1900And1970();
        }else if(1970 <= gameAge){
            over1970();
        }
        return age;
    }

    private void over1970() {
        age += 1;
    }

    private void between1900And1970() {
        age += 5;
    }

    private void between1750And1900() {
        age += 25;
    }

    private void between50And1750() {
        age += 50;
    }

    private void between100BCAnd50AD() {
        if(age == -100){
            age = -1;
        }else if(age == -1){
            age = 1;
        }else if (age == 1){
            age = 50;
        }
    }

    private void between4000And100BC() {
        age += 100;
    }
}

