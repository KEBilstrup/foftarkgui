package hotciv.semiCivStrategies;

import hotciv.framework.*;
import hotciv.standard.GameImpl;
import hotciv.variables.strategies.AttackStrategies;

import java.util.Random;

/**
 * Created by magni on 25-11-2016.
 */
public class EpsilonAttackStrategy implements AttackStrategies {

    private boolean test;
    private int diceValue;

    public EpsilonAttackStrategy(){
        test = false;
    }

    public EpsilonAttackStrategy(boolean test, int diceValue){
        this.test = test;
        this.diceValue = diceValue;
    }

    @Override
    public boolean attack(Position attacking, Position defending, Game game) {

        Unit attackingUnit = game.getUnitAt(attacking);
        Unit defendingUnit = game.getUnitAt(defending);
        GameImpl gameImpl = (GameImpl) game;

        if (calculateStrength(attacking, game, "attack") > calculateStrength(defending, game, "defend")) {

            if (gameImpl.getPlayerInTurn() == Player.BLUE) {
                gameImpl.incrementPlayerBlueAttacksWins();
            }
            if (gameImpl.getPlayerInTurn() == Player.RED) {
                gameImpl.incrementPlayerRedAttacksWins();
            }

            gameImpl.setUnit(defending, gameImpl.getUnitAt(attacking));
            gameImpl.setUnit(attacking, null);
            return true;
        } else
            return false;
    }

    public int calculateStrength(Position p, Game game, String move) {
        GameImpl gameImpl = (GameImpl) game;
        int strength = 0;

        Unit theUnit = game.getUnitAt(p);
        Tile tile = gameImpl.getTileAt(p);
        City city = gameImpl.getCityAt(p);

        if (move.equals("attack")){
            strength = theUnit.getAttackingStrength();
        }else if (move.equals("defend")){
            strength = theUnit.getDefensiveStrength();
        }

        int x = p.getRow();
        int y = p.getColumn();
        int friendlyUnitsCount = 0;

        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                Unit unit = gameImpl.getUnitAt(new Position(i, j));
                if (unit != null && unit.getOwner().equals(theUnit.getOwner())) {
                    friendlyUnitsCount++;
                }
            }
        }

        strength += friendlyUnitsCount - 1;
        if (tile.getTypeString().equals(GameConstants.FOREST) || tile.getTypeString().equals(GameConstants.HILLS)) {
            strength = strength * 2;
        }

        if (city != null) {
            strength = strength * 3;
        }

        if (test){
            strength = strength*diceValue;
        }

        if (!test){
            Random dice = new Random();
            strength = strength*dice.nextInt(6)+1;
        }

        return strength;
    }

    public void setDiceValue(int diceValue) {
        this.diceValue = diceValue;
    }
}

