package hotciv.tests;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.standard.*;
import hotciv.variables.SemiFactory;
import  hotciv.semiCivStrategies.EpsilonAttackStrategy;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by Karl-Emil on 02-12-2016.
 */
@SuppressWarnings("Duplicates")
public class TestSemiCiv {
    private Game game;
    private SemiFactory semiFactory;
    private ObserverStub observer;

    @Before //DDER KAN TILFØJES MERE HER, FX SPILLERE!!!!!!
    public void setUp() {
        semiFactory = new SemiFactory();
        game = new GameImpl(semiFactory);
        observer = new ObserverStub();
        game.addObserver(observer);
    }


    //copy pasta from BetaCiv
    @Test
    public void hundredYearsShouldPassPerTurnBetween4000BCAnd100BC(){
        GameImpl gameImpl = (GameImpl) game;
        gameImpl.setGameAge(-4000);

        assertThat(game.getAge(), is(-4000));
        game.endOfTurn();
        assertThat(game.getAge(), is(-3900));
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getAge(), is(-3700));

        gameImpl.setGameAge(-200);
        game.endOfTurn();
        assertThat(game.getAge(), is(-100));
    }
    //copy pasta from BetaCiv

    @Test
    public void shouldHaveSpecialSequenceAroundBirthOfChrist(){
        GameImpl gameImpl = (GameImpl) game;
        gameImpl.setGameAge(-100);

        assertThat(game.getAge(), is(-100));
        game.endOfTurn();
        assertThat(game.getAge(),is(-1));
        game.endOfTurn();
        assertThat(game.getAge(), is(1));
        game.endOfTurn();
        assertThat(game.getAge(), is(50));
    }
    // -100, -1, +1, +50
    //copy pasta from BetaCiv

    @Test
    public void between50ADAnd1750_50YearsPass(){
        GameImpl gameImpl = (GameImpl) game;
        gameImpl.setGameAge(50);

        assertThat(game.getAge(), is(50));
        game.endOfTurn();
        assertThat(game.getAge(), is(100));
        game.endOfTurn();
        assertThat(game.getAge(), is(150));

        gameImpl.setGameAge(1700);
        game.endOfTurn();
        assertThat(game.getAge(), is(1750));
    }
    //copy pasta from BetaCiv

    @Test
    public void between1750And1900_25YearsPass(){
        GameImpl gameImpl = (GameImpl) game;
        gameImpl.setGameAge(1750);

        assertThat(game.getAge(), is(1750));
        game.endOfTurn();
        assertThat(game.getAge(), is(1775));

        gameImpl.setGameAge(1875);
        game.endOfTurn();
        assertThat(game.getAge(), is(1900));
    }
    //copy pasta from BetaCiv

    @Test
    public void between1900And1970_5YearsPass(){
        GameImpl gameImpl = (GameImpl) game;
        gameImpl.setGameAge(1900);

        assertThat(game.getAge(), is(1900));
        game.endOfTurn();
        assertThat(game.getAge(), is(1905));

        gameImpl.setGameAge(1965);
        game.endOfTurn();
        assertThat(game.getAge(), is(1970));
    }
    //copy pasta from BetaCiv

    @Test
    public void after1970Only1YearPass(){
        GameImpl gameImpl = (GameImpl) game;
        gameImpl.setGameAge(1970);

        assertThat(game.getAge(), is(1970));
        game.endOfTurn();
        assertThat(game.getAge(), is(1971));

        gameImpl.setGameAge(2999);
        game.endOfTurn();
        assertThat(game.getAge(), is(3000)); // I'VE BEEN TO THE YEAR 3000, NOT MUCH HAS CHANGED BUT THEY LIVE UNDER WATER

    }

    //copypasta from GammaCiv
    @Test
    public void settlersShouldBuildCity() {
        Position settlerPosition = new Position(3, 3);
        PlayerUnit settler = new PlayerUnit(Player.RED, GameConstants.SETTLER);
        GameImpl gameImpl = (GameImpl) game;
        gameImpl.setUnit(settlerPosition, settler);

        assertThat(game.getUnitAt(settlerPosition), is(not(nullValue())));

        game.performUnitActionAt(settlerPosition);

        assertThat(game.getUnitAt(settlerPosition), is(nullValue()));

        assertThat(game.getCityAt(settlerPosition), is(not(nullValue())));
        assertThat(game.getCityAt(settlerPosition).getOwner(), is(settler.getOwner()));
    }

    //copypasta from GammaCiv

    @Test
    public void archerShouldDoubleDefensiveStrengthWhenFortifyNadReverse() {
        Position archerPosition = new Position(2, 2);
        ArcherUnit archer = new ArcherUnit(Player.RED, GameConstants.ARCHER);
        assertThat(archer.getDefensiveStrength(), is(3));

        GameImpl gameImpl = (GameImpl) game;
        gameImpl.setUnit(archerPosition, archer);

        game.performUnitActionAt(archerPosition);

        assertThat(archer.getDefensiveStrength(), is(6));

        game.performUnitActionAt(archerPosition);

        assertThat(archer.getDefensiveStrength(), is(3));

    }

    //copypasta from GammaCiv
    @Test
    public void archerCannotMoveIfFortified() {
        Position archerPosition = new Position(2, 2);
        ArcherUnit archer = new ArcherUnit(Player.RED, GameConstants.ARCHER);


        GameImpl gameImpl = (GameImpl) game;
        gameImpl.setUnit(archerPosition, archer);

        game.performUnitActionAt(archerPosition);

        game.moveUnit(archerPosition, new Position(5, 5));

        assertThat(gameImpl.getUnitAt(new Position(5, 5)), is(nullValue()));

    }
    //copypasta from DeltaCiv
    @Test
    public void blueAndRedShouldHaveCitiesAt4_5And8_12(){

        assertThat(game.getCityAt(new Position(4,5)).getOwner(),is(Player.BLUE));
        assertThat(game.getCityAt(new Position(8,12)).getOwner(),is(Player.RED));

    }
    //copypasta fra epsilon civ

    @Test
    public void winnerIsTheFirstPlayerToWinThreeAttacksRedWins(){
        GameImpl gameImpl = (GameImpl) game;
        semiFactory.setAttackStrategies(new EpsilonAttackStrategy(true, 1));

        Position archerPosition1 = new Position(1, 3);
        Position archerPosition2 = new Position(4, 4);
        Position archerPosition3 = new Position(6, 6);
        ArcherUnit archer = new ArcherUnit(Player.BLUE, GameConstants.ARCHER);

        gameImpl.setUnit(archerPosition1, archer);
        gameImpl.setUnit(archerPosition2, archer);
        gameImpl.setUnit(archerPosition3, archer);

        Position legionPosition = new Position(1, 2);
        Position legionPosition2 = new Position(4, 3);
        Position legionPosition3 = new Position(6, 5);
        LegionUnit legion = new LegionUnit(Player.RED, GameConstants.LEGION);

        gameImpl.setUnit(legionPosition, legion);
        gameImpl.setUnit(legionPosition2, legion);
        gameImpl.setUnit(legionPosition3, legion);

        gameImpl.moveUnit(legionPosition, archerPosition1);
        assertThat(gameImpl.getPlayerRedAttacksWins(), is(1));

        game.endOfTurn();
        game.endOfTurn();

        gameImpl.moveUnit(legionPosition2, archerPosition2);

        game.endOfTurn();
        game.endOfTurn();

        assertThat(gameImpl.getPlayerRedAttacksWins(), is(2));
        gameImpl.moveUnit(legionPosition3, archerPosition3);

        assertThat(gameImpl.getPlayerRedAttacksWins(), is(3));
        assertThat(Player.RED, is(game.getWinner()));
    }
    //copypasta fra epsilon civ

    @Test
    public void unitWithLowestCombinedAttackStrengthLosesTheAttack(){
        GameImpl gameImpl = (GameImpl) game;
        semiFactory.setAttackStrategies(new EpsilonAttackStrategy(true, 1));

        ArcherUnit archer = new ArcherUnit(Player.RED, GameConstants.ARCHER);
        Position attackingPosition = new Position(3,3);
        gameImpl.setUnit(attackingPosition, archer);

        LegionUnit legion = new LegionUnit(Player.BLUE, GameConstants.LEGION);
        Position defendingPosition = new Position(2,2);
        gameImpl.setUnit(defendingPosition, legion);

        gameImpl.moveUnit(attackingPosition, defendingPosition);

        assertThat(gameImpl.getUnitAt(defendingPosition).getOwner(), is(Player.BLUE));
    }
    //copypasta fra epsilon civ

    @Test
    public void attackingStrengthOfUnitIsCalculatedFromOwnStrengthAndTerrain(){
        GameImpl gameImpl = (GameImpl) game;

        EpsilonAttackStrategy epsilonAttackStrategy = new EpsilonAttackStrategy(true, 1);

        ArcherUnit archer = new ArcherUnit(Player.RED, GameConstants.ARCHER);
        Position attackingPosition = new Position(3,3);
        gameImpl.setUnit(attackingPosition, archer);
        assertThat(epsilonAttackStrategy.calculateStrength(attackingPosition, game, "attack"), is(2));

        gameImpl.setTile(attackingPosition, new WorldTile(GameConstants.FOREST));

        assertThat(epsilonAttackStrategy.calculateStrength(attackingPosition, game, "attack"), is(4));
    }
    //copypasta fra epsilon civ

    @Test
    public void defendingStrengthOfUnitIsCalculatedFromOwnStrengthAndTerrain(){
        GameImpl gameImpl = (GameImpl) game;

        EpsilonAttackStrategy epsilonAttackStrategy = new EpsilonAttackStrategy(true, 1);

        ArcherUnit archer = new ArcherUnit(Player.RED, GameConstants.ARCHER);
        Position defendingPosition = new Position(3,3);
        gameImpl.setUnit(defendingPosition, archer);
        assertThat(epsilonAttackStrategy.calculateStrength(defendingPosition, game, "defend"), is(3));

        gameImpl.setTile(defendingPosition, new WorldTile(GameConstants.FOREST));

        assertThat(epsilonAttackStrategy.calculateStrength(defendingPosition, game, "defend"), is(6));
    }
    //copypasta fra epsilon civ

    @Test
    public void attackingStrengthOfUnitIsCalculatedFromCity(){
        GameImpl gameImpl = (GameImpl) game;

        EpsilonAttackStrategy epsilonAttackStrategy = new EpsilonAttackStrategy(true, 1);

        ArcherUnit archer = new ArcherUnit(Player.RED, GameConstants.ARCHER);
        Position attackingPosition = new Position(3,3);
        gameImpl.setUnit(attackingPosition, archer);
        assertThat(epsilonAttackStrategy.calculateStrength(attackingPosition, game, "attack"), is(2));

        gameImpl.setCity(attackingPosition, new PlayerCity(Player.RED));

        assertThat(epsilonAttackStrategy.calculateStrength(attackingPosition, game, "attack"), is(6));
    }
    //copypasta fra epsilon civ

    @Test
    public void attackingStrengthOfUnitIsCalculatedFromAdjacent(){
        GameImpl gameImpl = (GameImpl) game;

        EpsilonAttackStrategy epsilonAttackStrategy = new EpsilonAttackStrategy(true, 1);

        ArcherUnit archer = new ArcherUnit(Player.RED, GameConstants.ARCHER);
        Position attackingPosition = new Position(3,3);
        gameImpl.setUnit(attackingPosition, archer);

        assertThat(epsilonAttackStrategy.calculateStrength(attackingPosition, game, "attack"), is(2));

        Position adjacentPosition1 = new Position(4,4);
        gameImpl.setUnit(adjacentPosition1, archer);

        assertThat(epsilonAttackStrategy.calculateStrength(attackingPosition, game, "attack"), is(3));

        Position adjacentPosition2 = new Position(2,3);
        gameImpl.setUnit(adjacentPosition2, archer);

        assertThat(epsilonAttackStrategy.calculateStrength(attackingPosition, game, "attack"), is(4));
    }
    //copypasta fra epsilon civ

    @Test
    public void defendingStrengthOfUnitDependsOnTileAndAdjacentAndCity(){
        GameImpl gameImpl = (GameImpl) game;

        EpsilonAttackStrategy epsilonAttackStrategy = new EpsilonAttackStrategy(true, 1);

        ArcherUnit archer = new ArcherUnit(Player.RED, GameConstants.ARCHER);
        Position defendingPosition = new Position(3,3);
        gameImpl.setUnit(defendingPosition, archer);

        assertThat(epsilonAttackStrategy.calculateStrength(defendingPosition, game, "defend"), is(3));

        Position adjacentPosition1 = new Position(4,4);
        gameImpl.setUnit(adjacentPosition1, archer);

        assertThat(epsilonAttackStrategy.calculateStrength(defendingPosition, game, "defend"), is(4));

        gameImpl.setCity(defendingPosition, new PlayerCity(Player.RED));

        assertThat(epsilonAttackStrategy.calculateStrength(defendingPosition, game, "defend"), is(12));

        gameImpl.setTile(defendingPosition, new WorldTile(GameConstants.HILLS));

        assertThat(epsilonAttackStrategy.calculateStrength(defendingPosition, game, "defend"), is(24));
    }
    //copypasta fra epsilon civ
    @Test
    public void defendingStrengthOfUnitDependsOnDiceValue(){
        GameImpl gameImpl = (GameImpl) game;

        EpsilonAttackStrategy epsilonAttackStrategy = new EpsilonAttackStrategy(true, 4);

        ArcherUnit archer = new ArcherUnit(Player.RED, GameConstants.ARCHER);
        Position defendingPosition = new Position(3,3);
        gameImpl.setUnit(defendingPosition, archer);

        assertThat(epsilonAttackStrategy.calculateStrength(defendingPosition, game, "defend"), is(3*4));

        epsilonAttackStrategy.setDiceValue(6);

        assertThat(epsilonAttackStrategy.calculateStrength(defendingPosition, game, "defend"), is(3*6));
    }

}
