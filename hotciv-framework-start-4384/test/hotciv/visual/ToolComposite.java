package hotciv.visual;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karl-Emil on 20-12-2016.
 */
public class ToolComposite implements CompositionToolComponent {
    List<CompositionToolComponent> tools = new ArrayList<CompositionToolComponent>();

    @Override
    public void addTool(CompositionToolComponent tool) {
        tools.add(tool);
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
        for (CompositionToolComponent tool: tools) {
            tool.mouseDown(mouseEvent, i, i1);
        }
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {
        for (CompositionToolComponent tool: tools) {
            tool.mouseDrag(mouseEvent, i, i1);
        }
    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {
        for (CompositionToolComponent tool: tools) {
            tool.mouseUp(mouseEvent, i, i1);
        }
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {
        for (CompositionToolComponent tool: tools) {
            tool.mouseMove(mouseEvent, i, i1);
        }
    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {
        for (CompositionToolComponent tool: tools) {
            tool.keyDown(keyEvent, i);
        }
    }
}
