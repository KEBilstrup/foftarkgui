package hotciv.standard;

import hotciv.framework.*;
import hotciv.variables.AbstractFactory;

import java.util.ArrayList;

/**
 * Skeleton implementation of HotCiv.
 * <p>
 * This source code is from the book
 * "Flexible, Reliable Software:
 * Using Patterns and Agile Development"
 * published 2010 by CRC Press.
 * Author:
 * Henrik B Christensen
 * Department of Computer Science
 * Aarhus University
 * <p>
 * Please visit http://www.baerbak.com/ for further information.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class GameImpl implements Game {
    public Player playerInTurn;
    private Tile[][] tilesMap;
    private Unit[][] unitsMap;
    private City[][] citiesMap;

    private int gameAge;
    private int playerBlueAttacksWins = 0;
    private int playerRedAttacksWins = 0;
    private int rounds = 0;

    private GameObserver observer;

    private AbstractFactory factory;

    public GameImpl(AbstractFactory factory) {

        this.factory = factory;

        gameAge = -4000;

        playerInTurn = Player.RED;

        initCities();
        initTiles();
        initUnit();
    }

    private void mapObserver() {
        for (int i = 16; i > 0; i--) {
            for (int j = 16; j > 0; j--) {
                observer.worldChangedAt(new Position(j, i));
            }
        }
    }

    private void initCities() {

        citiesMap = factory.getInitCitiesStrategies().initCities();

    }

    private void initUnit() {
        unitsMap = new Unit[16][16];
        unitsMap[4][3] = new SettlerUnit(Player.BLUE, GameConstants.SETTLER);
        unitsMap[2][0] = new ArcherUnit(Player.RED, GameConstants.ARCHER);
        unitsMap[3][2] = new LegionUnit(Player.BLUE, GameConstants.LEGION);
    }

    private void initTiles() {
        tilesMap = factory.getBuildWorldStrategy().buildWorld();
    }

    public Tile getTileAt(Position p) {
        return tilesMap[p.getRow()][p.getColumn()];
    }

    public Unit getUnitAt(Position p) {
        return unitsMap[p.getRow()][p.getColumn()];
    }

    public City getCityAt(Position p) {
        return citiesMap[p.getRow()][p.getColumn()];
    }

    public Player getPlayerInTurn() {
        return playerInTurn;
    }

    public Player getWinner() {
        //Not sure if it is alright to give the strategy the whole game object
        return factory.getWinningStrategy().getWinner(this);
    }

    public int getAge() {
        return gameAge;
    }

    public boolean moveUnit(Position from, Position to) {
        boolean legalMove = factory.getLegalMoveStrategy().isLegalMove(this, from, to);

        if (legalMove) {

            //ATTACK!!!
            if (getUnitAt(to) != null) {
                factory.getAttackStrategies().attack(from, to, this);
            } else {
                //Move unit
                unitsMap[to.getRow()][to.getColumn()] = unitsMap[from.getRow()][from.getColumn()];
                unitsMap[from.getRow()][from.getColumn()] = null;
            }

            PlayerUnit playerUnit = (PlayerUnit) getUnitAt(to);
            playerUnit.decrementMoveCount();
        }

        observer.worldChangedAt(from);
        observer.worldChangedAt(to);
        return true;
    }

    public void endOfTurn() {
        if (playerInTurn == Player.RED) {
            playerInTurn = Player.BLUE;
        } else {
            playerInTurn = Player.RED;
        }
//        worldAgingStrategy.setAge(getAge());
        gameAge = factory.getWorldAgingStrategy().calculateAge(gameAge);
        observer.turnEnds(playerInTurn, gameAge);
        setStockForAllCities();
        makeProductionForAllCities();

        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                if(getUnitAt(new Position(j,i)) != null){
                    PlayerUnit playerUnit = (PlayerUnit) getUnitAt(new Position(j,i));
                        playerUnit.resetMoveCount();
                    }
                }
            }

        rounds++;
    }

    private void makeProductionForAllCities() {
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                if (citiesMap[i][j] != null) {
                    PlayerCity city = (PlayerCity) citiesMap[i][j];
                    if (city.getProduction() != null) {

                        PlayerUnit unit;

                        if (city.getProduction().equals(GameConstants.ARCHER)) {
                            unit = new ArcherUnit(city.getOwner(), city.getProduction());
                        } else if (city.getProduction().equals(GameConstants.LEGION)) {
                            unit = new LegionUnit(city.getOwner(), city.getProduction());
                        } else if (city.getProduction().equals(AdditionalGameConstants.BOMB)) {
                            unit = new BombUnit(city.getOwner(), city.getProduction());
                        } else {
                            unit = new SettlerUnit(city.getOwner(), city.getProduction());
                        }

                        if (unit.getProductionCost() <= city.getStock()) {

                            if (getUnitAt(new Position(i, j)) == null) {
                                setUnit(new Position(i, j), unit);
                                city.setStock(city.getStock() - unit.getProductionCost());
                            } else {
                                if (getUnitAt(new Position(i - 1, j)) == null) {
                                    setUnit(new Position(i - 1, j), unit);
                                    city.setStock(city.getStock() - unit.getProductionCost());
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    private void setStockForAllCities() {
        for (City c : getAllCities()) {
            PlayerCity pC = (PlayerCity) c;
            pC.setStock(pC.getStock() + 6);
        }
    }

    public void changeWorkForceFocusInCityAt(Position p, String balance) {
    }

    public void changeProductionInCityAt(Position p, String unitType) {
        PlayerCity city = (PlayerCity) getCityAt(p);
        city.changeProduction(unitType);
    }

    public void performUnitActionAt(Position p) {
        if (getUnitAt(p).getOwner() != getPlayerInTurn()) {
            return;
        }
        factory.getPerformActionStrategies().performActionOn(this, p);
        mapObserver();
    }

    @Override
    public void addObserver(GameObserver observer) {
        this.observer = observer;
    }

    @Override
    public void setTileFocus(Position position) {
        observer.tileFocusChangedAt(position);
    }

    public ArrayList<City> getAllCities() {
        ArrayList<City> cities = new ArrayList();
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                if (citiesMap[i][j] != null) {
                    cities.add(citiesMap[i][j]);
                }
            }
        }
        return cities;
    }

    public int getRounds() {
        return rounds;
    }

    /**
     * Be carefull
     *
     * @param p
     * @param unit
     */
    public void setUnit(Position p, Unit unit) {
        unitsMap[p.getRow()][p.getColumn()] = unit;
    }

    public void incrementPlayerBlueAttacksWins() {
        playerBlueAttacksWins++;
    }

    public void incrementPlayerRedAttacksWins() {
        playerRedAttacksWins++;
    }

    public void resetPlayerAttacksWins() {
        playerBlueAttacksWins = 0;
        playerRedAttacksWins = 0;
    }

    /**
     * Be carefull
     *
     * @param p
     * @param city
     */
    public void setCity(Position p, City city) {
        citiesMap[p.getRow()][p.getColumn()] = city;
    }

    public void setTile(Position p, Tile tile) {
        tilesMap[p.getRow()][p.getColumn()] = tile;
    }

    /**
     * For test purposes only
     *
     * @param gameAge
     */

    public void setGameAge(int gameAge) {
        this.gameAge = gameAge;
    }

    public int getPlayerBlueAttacksWins() {
        return playerBlueAttacksWins;
    }

    public int getPlayerRedAttacksWins() {
        return playerRedAttacksWins;
    }


    public GameObserver getObserver() {
        return observer;
    }
}