package hotciv.stub;

import hotciv.framework.*;
import hotciv.standard.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ThinkNick on 20-12-2016.
 */
public class StubAction implements Game {
    private final Position pos_archer_red;
    private final Position pos_legion_blue;
    private final Position pos_settler_red;
    private final Position pos_bomb_red;
    private final StubArcher red_archer;
    private final StubSettler red_settler;
    private City[][] citiesMap;
    private Tile[][] tilesMap;
    private Unit[][] unitsMap;

    private GameObserver observer;

    public StubAction() {
        defineWorld(1);
        // AlphaCiv configuration
        pos_archer_red = new Position( 2, 0);
        pos_legion_blue = new Position( 3, 2);
        pos_settler_red = new Position( 4, 3);
        pos_bomb_red = new Position( 6, 4);

        // the only one I need to store for this stub
        red_archer = new StubArcher(Player.RED);
        red_settler = new StubSettler(Player.RED);

        initCities();
        initUnit();
    }

    protected Map<Position,Tile> world;
    @Override
    public Tile getTileAt( Position p ) { return world.get(p); }

    protected void defineWorld(int worldType) {
        world = new HashMap<Position,Tile>();
        for ( int r = 0; r < GameConstants.WORLDSIZE; r++ ) {
            for ( int c = 0; c < GameConstants.WORLDSIZE; c++ ) {
                Position p = new Position(r,c);
                world.put( p, new StubTile(GameConstants.PLAINS));
            }
        }

    }
    private void initCities() {

        citiesMap = new City[16][16];

    }

    private void initUnit() {
        unitsMap = new Unit[16][16];

        unitsMap[pos_settler_red.getRow()][pos_settler_red.getColumn()] = red_settler;
        unitsMap[pos_archer_red.getRow()][pos_archer_red.getColumn()] = red_archer;


        unitsMap[pos_legion_blue.getRow()][pos_legion_blue.getColumn()] = new StubUnit( GameConstants.LEGION, Player.BLUE );
        unitsMap[pos_bomb_red.getRow()][pos_bomb_red.getColumn()] = new StubUnit( ThetaConstants.BOMB, Player.RED );

    }

    @Override
    public Unit getUnitAt(Position p) {
        return unitsMap[p.getRow()][p.getColumn()];
    }


    @Override
    public City getCityAt(Position p) {
        return citiesMap[p.getRow()][p.getColumn()];
    }

    @Override
    public Player getPlayerInTurn() {
        return null;
    }

    @Override
    public Player getWinner() {
        return null;
    }

    @Override
    public int getAge() {
        return 0;
    }

    @Override
    public boolean moveUnit(Position from, Position to) {
        return false;
    }

    @Override
    public void endOfTurn() {

    }

    @Override
    public void changeWorkForceFocusInCityAt(Position p, String balance) {

    }

    @Override
    public void changeProductionInCityAt(Position p, String unitType) {

    }

    @Override
    public void performUnitActionAt(Position p) {
        if (getUnitAt(p)!= null) {
            Player owner = getUnitAt(p).getOwner();

            if (getUnitAt(p).getTypeString().equals(GameConstants.SETTLER)) {
                System.out.println("YOU PERFORMED STUFF ON A SETTLER");
                setUnit(p, null);
                setCity(p, new StubCity(owner));
                observer.worldChangedAt(p);
                return;
            }

            if (getUnitAt(p).getTypeString().equals(GameConstants.ARCHER)) {
                StubArcher archerUnit = (StubArcher) getUnitAt(p);
                if (!archerUnit.isFortified()) {
                    archerUnit.fortified(true);
                    observer.worldChangedAt(p);
                    return;
                } else {
                    archerUnit.fortified(false);
                    observer.worldChangedAt(p);
                    return;
                }
            }

            if (getUnitAt(p).getTypeString().equals(AdditionalGameConstants.BOMB)) {
                int x = p.getRow();
                int y = p.getColumn();
                for (int i = x - 1; i <= x + 1; i++) {
                    for (int j = y - 1; j <= y + 1; j++) {
                        setUnit(new Position(i,j), null);
                    }
                }
                observer.worldChangedAt(p);
                return;
            }
        }
    }

    @Override
    public void addObserver(GameObserver observer)
    {
        this.observer = observer;
    }

    @Override
    public void setTileFocus(Position position) {

    }

    public GameObserver getObserver() {
        return observer;
    }
    private void mapObserver() {
        for (int i = 16; i > 0; i--) {
            for (int j = 16; j > 0; j--) {
                observer.worldChangedAt(new Position(j, i));
            }
        }
    }
    public void setCity(Position p, City city) {
        citiesMap[p.getRow()][p.getColumn()] = city;
    }

    public void setTile(Position p, Tile tile) {
        tilesMap[p.getRow()][p.getColumn()] = tile;
    }
    public void setUnit(Position p, Unit unit) {
        unitsMap[p.getRow()][p.getColumn()] = unit;
    }

}
