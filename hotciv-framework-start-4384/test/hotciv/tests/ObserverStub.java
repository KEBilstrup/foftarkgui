package hotciv.tests;

import hotciv.framework.GameObserver;
import hotciv.framework.Player;
import hotciv.framework.Position;

/**
 * Created by Karl-Emil on 15-12-2016.
 */
public class ObserverStub implements GameObserver {
    public Position worldChangedAt;
    public Player playerInTurn;
    public int age;
    public Position tileFocusChangedAt;
    public int worldChangedAtCount = 0;

    @Override
    public void worldChangedAt(Position pos) {
        worldChangedAt = pos;
        worldChangedAtCount++;
    }

    @Override
    public void turnEnds(Player nextPlayer, int age) {
        playerInTurn = nextPlayer;
        this.age = age;
    }

    @Override
    public void tileFocusChangedAt(Position position) {
        tileFocusChangedAt = position;
    }
}
