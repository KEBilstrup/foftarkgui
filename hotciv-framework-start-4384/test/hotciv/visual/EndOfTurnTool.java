package hotciv.visual;

import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.view.CivDrawing;
import hotciv.view.GfxConstants;
import minidraw.framework.DrawingEditor;
import minidraw.framework.Figure;
import minidraw.framework.Tool;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Created by Karl-Emil on 16-12-2016.
 */
public class EndOfTurnTool implements CompositionToolComponent {
    Game game;


    public EndOfTurnTool(Game game) {
        this.game = game;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
        System.out.println(i + " " + i1);
        Position p = GfxConstants.getPositionFromXY(i, i1);
        if (isBetween(i, GfxConstants.TURN_SHIELD_X, GfxConstants.TURN_SHIELD_X + 20)) {
            if (isBetween(i1, GfxConstants.TURN_SHIELD_Y, GfxConstants.TURN_SHIELD_Y + 40)) {
                game.endOfTurn();
            }
        }
    }

    public static boolean isBetween(int value, int min, int max) {
        return ((value > min) && (value < max));
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {

    }

    @Override
    public void addTool(CompositionToolComponent tool) {
        throw new NotImplementedException();
    }
}

