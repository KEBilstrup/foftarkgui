package hotciv.semiCivStrategies;

import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.standard.GameImpl;
import hotciv.variables.strategies.WinningStrategies;

/**
 * Created by magni on 24-11-2016.
 */
public class EpsilonWinningStrategy implements WinningStrategies {
    @Override
    public Player getWinner(Game game) {
        GameImpl gameImpl = (GameImpl) game;
        if (gameImpl.getPlayerBlueAttacksWins() == 3) {return Player.BLUE;}
        if (gameImpl.getPlayerRedAttacksWins() == 3) {return Player.RED;}
        return null;
    }
}
