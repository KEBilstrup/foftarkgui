package hotciv.stub;

import hotciv.framework.*;
import hotciv.standard.*;

import java.util.HashMap;
import java.util.Map;

import static hotciv.framework.Player.RED;

/**
 * Created by ThinkNick on 19-12-2016.
 */
public class StupGameEndTurn implements Game {
    private int gameAge;
    private final Position pos_archer_red;
    private final Position pos_legion_blue;
    private final Position pos_settler_red;
    private final Position pos_bomb_red;
    private final StubUnit red_archer;
    public Player playerInTurn;
    private GameObserver observer;
    private int rounds = 0;

    public StupGameEndTurn(){
        defineWorld(1);
        // AlphaCiv configuration
        pos_archer_red = new Position( 2, 0);
        pos_legion_blue = new Position( 3, 2);
        pos_settler_red = new Position( 4, 3);
        pos_bomb_red = new Position( 6, 4);

        // the only one I need to store for this stub
        red_archer = new StubUnit( GameConstants.ARCHER, Player.RED );
        playerInTurn = RED;
        gameAge = -4000;

    }

    public void endOfTurn() {
        if (playerInTurn == RED) {
            playerInTurn = Player.BLUE;
        } else {
            playerInTurn = RED;
        }
        gameAge = gameAge + 100;
        observer.turnEnds(playerInTurn, gameAge);
    }

    protected Map<Position,Tile> world;
    @Override
    public Tile getTileAt( Position p ) { return world.get(p); }

    protected void defineWorld(int worldType) {
        world = new HashMap<Position,Tile>();
        for ( int r = 0; r < GameConstants.WORLDSIZE; r++ ) {
            for ( int c = 0; c < GameConstants.WORLDSIZE; c++ ) {
                Position p = new Position(r,c);
                world.put( p, new StubTile(GameConstants.PLAINS));
            }
        }
    }


    @Override
    public Unit getUnitAt(Position p) {
        if ( p.equals(pos_archer_red) ) {
            return red_archer;
        }
        if ( p.equals(pos_settler_red) ) {
            return new StubUnit( GameConstants.SETTLER, Player.RED );
        }
        if ( p.equals(pos_legion_blue) ) {
            return new StubUnit( GameConstants.LEGION, Player.BLUE );
        }
        if ( p.equals(pos_bomb_red) ) {
            return new StubUnit( ThetaConstants.BOMB, Player.RED );
        }
        return null;
    }

    @Override
    public City getCityAt(Position p) {
        return null;
    }

    @Override
    public Player getPlayerInTurn() {
        return playerInTurn;
    }

    @Override
    public Player getWinner() {
        return null;
    }

    @Override
    public int getAge() {
        return gameAge;
    }

    @Override
    public boolean moveUnit(Position from, Position to) {
        return false;
    }


    @Override
    public void changeWorkForceFocusInCityAt(Position p, String balance) {

    }

    @Override
    public void changeProductionInCityAt(Position p, String unitType) {

    }

    @Override
    public void performUnitActionAt(Position p) {

    }

    @Override
    public void addObserver(GameObserver observer) {
        this.observer = observer;
    }

    @Override
    public void setTileFocus(Position position) {

    }
    public GameObserver getObserver() {
        return observer;
    }
    private void mapObserver() {
        for (int i = 16; i > 0; i--) {
            for (int j = 16; j > 0; j--) {
                observer.worldChangedAt(new Position(j, i));
            }
        }
    }

}
