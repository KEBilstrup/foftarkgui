package hotciv.standard;

import hotciv.framework.Player;

/**
 * Created by Karl-Emil on 29-11-2016.
 */
public class SettlerUnit extends PlayerUnit {

    public SettlerUnit(Player owner, String type) {
        super(owner, type);
    }

    @Override
    public int getProductionCost() {
        return 30;
    }
}
