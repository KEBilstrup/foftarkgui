package hotciv.semiCivStrategies;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.standard.ArcherUnit;
import hotciv.variables.strategies.LegalMoveStrategy;

/**
 * Created by Karl-Emil on 20-12-2016.
 */
public class GammaLegalMoveStrategy implements LegalMoveStrategy {
    @Override
    public boolean isLegalMove(Game game, Position from, Position to) {
        LegalMoveStrategy alpaStra = new AlphaLegalMoveStrategy();
        if(!alpaStra.isLegalMove(game, from, to)){return false;}

        if (game.getUnitAt(from).getTypeString() == GameConstants.ARCHER) {
            ArcherUnit archer = (ArcherUnit) game.getUnitAt(from);
            if (archer.isFortified()) {
                return false;
            }
        }
        return true;
    }
}
