package hotciv.standard;

import hotciv.framework.City;
import hotciv.framework.Player;
import hotciv.framework.Position;

/**
 * Created by Karl-Emil on 04-11-2016.
 */
public class PlayerCity implements City {

    private Position cityPosition;
    private Player cityOwner;
    private int stock;
    private String unitType;


    public PlayerCity(Player owner){
        cityOwner = owner;
        unitType = "None";
    }

    @Override
    public Player getOwner() {
        return cityOwner;
    }

    public void setOwner(Player newOwner){
        cityOwner = newOwner;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public String getProduction() {
        return unitType;
    }

    @Override
    public String getWorkforceFocus() {
        return "Fake WorkForce";
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void changeProduction(String unitType){this.unitType = unitType;}
}
