package hotciv.stub;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Unit;

/**
 * Created by magni on 20-12-2016.
 */
public class StubSettler implements Unit {
    private Player owner;

    public StubSettler(Player owner){
        this.owner = owner;
    }

    @Override
    public String getTypeString() {
        return GameConstants.SETTLER;
    }

    @Override
    public Player getOwner() {
        return owner;
    }

    @Override
    public int getMoveCount() {
        return 1;
    }

    @Override
    public int getDefensiveStrength() {
        return 0;
    }

    @Override
    public int getAttackingStrength() {
        return 0;
    }
}
