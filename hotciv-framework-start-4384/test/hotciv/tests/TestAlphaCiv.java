package hotciv.tests;

import hotciv.framework.*;
import hotciv.standard.GameImpl;
import hotciv.standard.PlayerCity;
import hotciv.standard.PlayerUnit;
import hotciv.standard.WorldTile;
import hotciv.variables.SemiFactory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/** Skeleton class for alphaCiv test cases

 Updated Oct 2015 for using Hamcrest matchers

 This source code is from the book
 "Flexible, Reliable Software:
 Using Patterns and Agile Development"
 published 2010 by CRC Press.
 Author:
 Henrik B Christensen
 Department of Computer Science
 Aarhus University

 Please visit http://www.baerbak.com/ for further information.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 TODO:
 tilføj spillere og andet til @Before
 kommenter koden, når du rydder op
 Note exception tricks: @Test ( expected=I l l ega lCoinEx c ept ion . c l a s s )
 Skal måske bruges is tedet for at tjekke for null?
 Læse op på de forskellige assertions
 "get the assertions in place first and return to the problem of setting up the
 stuff to be asserted later."
 */
public class TestAlphaCiv {
    private Game game;
    private GameImpl gameImpl;
    private ObserverStub observer;

    /** Fixture for alphaciv testing. */
    @Before //DDER KAN TILFØJES MERE HER, FX SPILLERE!!!!!!
    public void setUp() {
        game = new GameImpl(new SemiFactory());
        gameImpl = (GameImpl) game;
        observer = new ObserverStub();
        game.addObserver(observer);
    }

    // FRS p. 455 states that 'Red is the first player to take a turn'.
    @Test
    public void shouldBeRedAsStartingPlayer() {
        assertThat(game.getPlayerInTurn(), is(Player.RED));
    }

    @Test
    public void shouldBeBlueInSecondTurn() {
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.BLUE));
    }

    @Test
    public void shouldChangePlayerOnEndOfTurn(){
        Player player = game.getPlayerInTurn();
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(not(player)));
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(player));
    }

    @Test
    public void shouldHaveRedCityAt1_1(){
        City redPlayerCity = game.getCityAt(new Position(1,1));
        assertThat(redPlayerCity.getOwner(), is(Player.RED));
    }

    @Test
    public void shouldHaveBlueCityAt4_1(){
        City bluePlayCity = game.getCityAt(new Position(4,1));
        assertThat(bluePlayCity.getOwner(), is(Player.BLUE));
    }

    @Test
    public void shouldHaveOceanAt1_0(){
        Tile oceanTile = game.getTileAt(new Position(1,0));
        assertThat(oceanTile.getTypeString(),is(GameConstants.OCEANS));
    }

    @Test
    public void shouldHaveMostlyPlainTiles(){
        assertThat(game.getTileAt(new Position(0,0)).getTypeString(), is(GameConstants.PLAINS));
        assertThat(game.getTileAt(new Position(7,7)).getTypeString(), is(GameConstants.PLAINS));
        assertThat(game.getTileAt(new Position(15,15)).getTypeString(), is(GameConstants.PLAINS));
    }

    @Test
    public void shouldHaveHillsAt0_1(){
        Tile hillsTile = game.getTileAt(new Position(0,1));
        assertThat(hillsTile.getTypeString(), is(GameConstants.HILLS));
    }

    @Test
    public void shouldHaveMountainsAt2_2(){
        Tile mountainsTile = game.getTileAt(new Position(2,2));
        assertThat(mountainsTile.getTypeString(), is(GameConstants.MOUNTAINS));
    }

    @Test
    public void shouldHave1CitizenInCities(){
        City playerCity = new PlayerCity(Player.RED);
        assertThat(playerCity.getSize(), is(1));
    }

    @Test
    public void should100YearsPassAfterEachTurn(){
        int age = game.getAge();
        game.endOfTurn();
        assertThat(age + 100, is(game.getAge()));
    }

    @Test
    public void gameStartsAtYear4000BC(){
        //5000 bc = -5000
        assertThat(game.getAge(), is(-4000));
    }

    @Test
    public void redShouldWinInYear3000(){
        assertThat(game.getWinner(), is(nullValue()));
        //10 runder = 1000 år, forløkken tæller op fra -4000 til -3000
        for(int i = 0; i < 10; i++ ) {
            game.endOfTurn();
        }
        assertThat(game.getWinner(), is(Player.RED));
    }

    @Test
    public void shouldBeARedsArcherAt2_0(){
        assertThat(game.getUnitAt(new Position(2,0)).getTypeString(), is(GameConstants.ARCHER));
        assertThat(game.getUnitAt(new Position(2,0)).getOwner(), is(Player.RED));
    }

    @Test
    public void shouldBeBlueLegionAt3_2(){
        Unit blueLegion = game.getUnitAt(new Position(3,2));
        assertThat(blueLegion.getTypeString(), is(GameConstants.LEGION));
        assertThat(blueLegion.getOwner(), is(Player.BLUE));
    }

    @Test
    public void shouldBeRedSettlerAt4_3(){
        Unit redSettler = game.getUnitAt(new Position(4,3));
        assertThat(redSettler.getTypeString(), is(GameConstants.SETTLER));
        assertThat(redSettler.getOwner(), is(Player.BLUE));
    }

    @Ignore
    public void redUnitAttackAndDestroyBlueUnit(){
        assertThat(game.getUnitAt(new Position(3,2)).getOwner(), is(Player.BLUE));
        //RED ATTACKS!!!
        game.performUnitActionAt(new Position(3,2));
        assertThat(game.getUnitAt(new Position(3,2)), is(nullValue()));
    }

    @Test
    public void unitShouldAttackAndDestroyOtherUnitWhenItMovesOnTopOfIt(){
        //Blue unit at 3,2
        Unit blueUnit= game.getUnitAt(new Position(3,2));
        assertThat(blueUnit.getOwner(), is(Player.BLUE));
        //Red unit at 2,0
        Unit redUnit = new PlayerUnit(Player.RED, GameConstants.ARCHER);
        ((GameImpl)game).setUnit(new Position(3,3), redUnit);
        assertThat(redUnit.getOwner(), is(Player.RED));
        //RED ATTACKS BLUE
        game.moveUnit(new Position(3,3), new Position(3,2));
        assertThat(game.getUnitAt(new Position(3,2)).getOwner(), is(Player.RED));
    }

    @Test
    public void unitsCanMove(){
        // blue unit
        game.endOfTurn();
        Position oldPosition = new Position(3,2);
        Position newPosition = new Position(3,3);
        Unit movableUnit = game.getUnitAt(oldPosition);

        assertThat(movableUnit, is(notNullValue()));
        assertThat(game.getUnitAt(newPosition), is(nullValue()));

        game.moveUnit(oldPosition, newPosition);

        assertThat(game.getUnitAt(newPosition), is(movableUnit));
        assertThat(game.getUnitAt(oldPosition), is(nullValue()));
    }


    @Test
    public void aUnitCannotMoveToAPositionWhereAnotherFriendlyUnitIs(){
        //So its blues turn
        game.endOfTurn();
        PlayerUnit unit1 = new PlayerUnit(Player.BLUE, GameConstants.LEGION);
        Position position1 = new Position(6,6);
        gameImpl.setUnit(position1, unit1);

        PlayerUnit unit2 = new PlayerUnit(Player.BLUE, GameConstants.ARCHER);
        Position position2= new Position(6,7);
        gameImpl.setUnit(position2, unit2);

        game.moveUnit(position1, position2);
        assertThat(game.getUnitAt(position1), is(notNullValue()));
        assertThat(game.getUnitAt(position2).getTypeString(), is(GameConstants.ARCHER));
    }

    @Test
    public void aUnitCannotMoveOverMountains(){
        //So its blues turn
        game.endOfTurn();
        PlayerUnit unit1 = new PlayerUnit(Player.BLUE, GameConstants.LEGION);
        Position position1 = new Position(6,6);
        gameImpl.setUnit(position1, unit1);

        Position position2 = new Position(6,7);
        gameImpl.setTile(position2, new WorldTile(GameConstants.MOUNTAINS));

        assertThat(game.getUnitAt(position2), is(nullValue()));

        game.moveUnit(position1, position2);

        assertThat(game.getUnitAt(position2), is(nullValue()));
        assertThat(game.getUnitAt(position1), is(notNullValue()));

    }
    @Test
    public void unitsMustWalk1Tile(){
        PlayerUnit unit = new PlayerUnit(Player.RED, GameConstants.ARCHER);
        Position unitP = new Position(7,7);
        ((GameImpl) game ).setUnit(unitP, unit);
        assertThat(game.moveUnit(unitP, new Position(7,9)), is(false));
        assertThat(game.getUnitAt(new Position(7,8)), is(nullValue()));
        assertThat(game.moveUnit(unitP, new Position(7,8)), is(true));
    }

    @Test
    public void redCannotMoveBluesUnits(){
        //Blue unit at 4,3
        Position oldPosition = new Position(4,3);
        Position newPosition = new Position(4,5);

        if( game.getPlayerInTurn() == Player.RED){
            assertThat(game.moveUnit(oldPosition,newPosition), is(false));
        } else if (game.getPlayerInTurn() == Player.BLUE){
            assertThat(game.moveUnit(oldPosition,newPosition), is(true));
        }
    }

    @Test
    public void shouldHave6ProductionInCity(){
        PlayerCity aCity = (PlayerCity) game.getCityAt(new Position(1,1));
        assertThat(aCity.getStock(), is(0));
        game.endOfTurn();
        assertThat(aCity.getStock(), is(6));
    }

    @Test
    public void shouldConquerRedCityWhenBlueUnitMovesThere(){
        PlayerCity cityRed = new PlayerCity(Player.RED);
        gameImpl.setCity(new Position(4,2), cityRed);
        //Blue should have turn:
        game.endOfTurn();
        //Blue unit at 4,3
        Unit blueUnit = game.getUnitAt(new Position(4,3));
        assertThat(game.getCityAt(new Position(4,2)).getOwner(), is(not(blueUnit.getOwner())));
        game.moveUnit(new Position(4,3), new Position(4,2));
        assertThat(game.getCityAt(new Position(4,2)).getOwner(), is(blueUnit.getOwner()));
    }

    @Test
    public void shouldChangeTheProductionTypeInACity(){
        PlayerCity aCity = new PlayerCity(Player.BLUE);
        assertThat(aCity.getProduction(), is(nullValue()));
        aCity.changeProduction(GameConstants.ARCHER);
        assertThat(aCity.getProduction(), is(GameConstants.ARCHER));
    }

    @Test
    public void shouldChangeTheProductionTypeFromGame(){
        PlayerCity aCity = new PlayerCity(Player.BLUE);
        Position aCityP = new Position(7,7);
        gameImpl.setCity(aCityP, aCity);
        game.changeProductionInCityAt(aCityP, GameConstants.ARCHER);
        assertThat(aCity.getProduction(), is(GameConstants.ARCHER));
        game.changeProductionInCityAt(aCityP, GameConstants.LEGION);
        assertThat(aCity.getProduction(), is(GameConstants.LEGION));
    }

    @Test
    public void citiesProduce6ProductionPerRound(){
        PlayerCity aCity = new PlayerCity(Player.BLUE);
        PlayerCity anotherCity = new PlayerCity(Player.BLUE);
        Position aCityP = new Position(7,7);
        Position anotherCityP = new Position(8,8);
        gameImpl.setCity(aCityP, aCity);
        gameImpl.setCity(anotherCityP, anotherCity);

        assertThat(aCity.getStock(), is(0));
        assertThat(anotherCity.getStock(), is(0));

        game.endOfTurn();

        assertThat(aCity.getStock(), is(6));
        assertThat(anotherCity.getStock(), is(6));

        game.endOfTurn();

        assertThat(aCity.getStock(), is(12));
    }


    @Test
    public void citiesProducesArcherIfEnoughInStock(){
        PlayerCity aCity = new PlayerCity(Player.BLUE);
        Position aCityP = new Position(7,7);
        gameImpl.setCity(aCityP, aCity);
        aCity.changeProduction(GameConstants.ARCHER);

        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getUnitAt(aCityP).getTypeString(), is(GameConstants.ARCHER));
        assertThat(aCity.getStock(), is(2));
    }

    @Test
    public void citiesProducesLegionIfEnoughInStock(){
        PlayerCity aCity = new PlayerCity(Player.BLUE);
        Position aCityP = new Position(7,7);
        gameImpl.setCity(aCityP, aCity);
        aCity.changeProduction(GameConstants.LEGION);

        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getUnitAt(aCityP).getTypeString(), is(GameConstants.LEGION));
        assertThat(aCity.getStock(), is(3));
    }

    @Test
    public void newUnitsShouldBePlacedToTheNorthIfCityIsOccupied(){
        PlayerCity aCity = new PlayerCity(Player.BLUE);
        Position aCityP = new Position(7,7);
        gameImpl.setCity(aCityP, aCity);
        aCity.changeProduction(GameConstants.LEGION);

        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getUnitAt(aCityP).getTypeString(), is(GameConstants.LEGION));

        Position northOfCityP = new Position(6,7);

        aCity.changeProduction(GameConstants.ARCHER);

        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getUnitAt(northOfCityP).getTypeString(), is(GameConstants.ARCHER));
    }

    @Test
    public void shouldAddObserver(){
        game.addObserver(observer);
        assertThat(gameImpl.getObserver(), is(notNullValue()));
    }

    @Test
    public void shouldNotifyObserverWhenUnitMove(){
        observer.worldChangedAtCount = 0; //reset count
        game.endOfTurn();
        Position oldPosition = new Position(3,2);
        Position newPosition = new Position(3,3);
        game.moveUnit(oldPosition, newPosition);
        assertThat(observer.worldChangedAtCount, is(2));
    }

    @Test
    public void shouldNotifObserverWhenChangeTileFocus(){
        Position pos = new Position(8,8);
        game.setTileFocus(pos);
        assertThat(observer.tileFocusChangedAt, is(pos));
    }

    @Test
    public void shouldNotifyObserverWhenTurnEnds(){
        game.endOfTurn();
        assertThat(observer.age, is(game.getAge()));
        assertThat(observer.playerInTurn, is(game.getPlayerInTurn()));
    }

    @Ignore
    public void shouldNotifyObserverWhenGameInitialize(){
        int number = 256;
        assertThat(observer.worldChangedAtCount, is(number));
        assertThat(observer.age, is(game.getAge()));
        assertThat(observer.playerInTurn, is(game.getPlayerInTurn()));
    }

}
