package hotciv.variables;

import hotciv.semiCivStrategies.*;
import hotciv.variables.strategies.*;

/**
 * Created by Karl-Emil on 29-11-2016.
 */
public class SemiFactory implements AbstractFactory{

    private WinningStrategies winningStrategy;
    private WorldAgingStrategies worldAgingStrategy;
    private PerformActionStrategies performActionStrategies;
    private InitCitiesStrategies initCitiesStrategies;
    private AttackStrategies attackStrategies;
    private BuildWorldStrategy buildWorldStrategy;
    private LegalMoveStrategy legalMoveStrategy;

    public SemiFactory(){
        worldAgingStrategy = new BetaAgingStrategy();
        initCitiesStrategies = new DeltaInitCityStrategy();
        attackStrategies = new EpsilonAttackStrategy();
        buildWorldStrategy = new AlphaBuildWorldStrategy();
        legalMoveStrategy = new GammaLegalMoveStrategy();
        performActionStrategies = new ThetaPerformActionStrategy();
        winningStrategy = new EpsilonWinningStrategy();

    }

    @Override
    public WinningStrategies getWinningStrategy() {
        return winningStrategy;
    }

    @Override
    public WorldAgingStrategies getWorldAgingStrategy() {
        return worldAgingStrategy;
    }

    @Override
    public PerformActionStrategies getPerformActionStrategies() {
        return performActionStrategies;
    }

    @Override
    public InitCitiesStrategies getInitCitiesStrategies() {
        return initCitiesStrategies;
    }

    @Override
    public AttackStrategies getAttackStrategies() {
        return attackStrategies;
    }

    @Override
    public BuildWorldStrategy getBuildWorldStrategy() {
        return buildWorldStrategy;
    }

    @Override
    public LegalMoveStrategy getLegalMoveStrategy() {
        return legalMoveStrategy;
    }

    public void setAttackStrategies(AttackStrategies attackStrategies) {
        this.attackStrategies = attackStrategies;
    }

}



