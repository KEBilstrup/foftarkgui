package hotciv.standard;

import hotciv.framework.Player;
import hotciv.framework.Unit;

/**
 * Created by Karl-Emil on 04-11-2016.
 */
public class PlayerUnit implements Unit {

    private final Player owner;
    private final String type;
    private int moveCount;

    private int strength;

    public PlayerUnit(Player owner, String type){
        this.owner = owner;
        this.type = type;
        moveCount = 1;
    }
    @Override
    public String getTypeString() {
        return type;
    }

    @Override
    public Player getOwner() {
        return owner;
    }

    @Override
    public int getMoveCount() {
        return moveCount;
    }

    public void decrementMoveCount(){
        moveCount--;
    }

    public void incrementMoveCount(){
        moveCount++;
    }

    @Override
    public int getDefensiveStrength() {return 0;}

    @Override
    public int getAttackingStrength() {
        return 0;
    }

    //Maybe not okay, see page 460
    public void setDefensiveStrength(int strength){
        this.strength = strength;
    }

    public int getProductionCost() {
        return 0;
    }

    public static int getValue(String type){
        return 0;
    }

    public void resetMoveCount() {
        moveCount = 1;
    }
}
