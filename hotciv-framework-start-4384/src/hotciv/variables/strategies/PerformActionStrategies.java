package hotciv.variables.strategies;

import hotciv.framework.Game;
import hotciv.framework.Position;

/**
 * Created by magni on 18-11-2016.
 */
public interface PerformActionStrategies {
        public void performActionOn(Game game, Position position);
}
